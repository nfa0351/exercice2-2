package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class T extends AbstractImageFrameMedia {

    /**
     * Permet d'obtenir le flux d'écriture sous-tendant à notre canal
     *
     * @return le flux d'écriture
     * @throws IOException
     */
    @Override
    public OutputStream getEncodedImageOutput() throws IOException {
        return null;
    }

    /**
     * Permet d'obtenir le flux de lecture sous-tendant à notre canal
     *
     * @return le flux de lecture
     * @throws IOException
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return null;
    }
}
