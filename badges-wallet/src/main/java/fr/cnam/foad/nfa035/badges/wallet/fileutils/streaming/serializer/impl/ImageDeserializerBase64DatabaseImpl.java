package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

/**
 * import de la classe ImageFrameMedia du package media
 */
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.DatabaseDeserializer;
import org.apache.commons.codec.binary.Base64InputStream;

import java.io.*;

public class ImageDeserializerBase64DatabaseImpl implements DatabaseDeserializer {

    private OutputStream souceOutputStream;

    /**
     * Méthode principale de désérialisation, consistant au transfert simple du flux de lecture vers un flux d'écriture
     *
     * @param media
     * @throws IOException
     */
    @Override
    public void deserialize(ImageFrameMedia media) throws IOException {
    }

    @Override
    public void deserialize(ResumableImageFrameMedia media) throws IOException {
        //TODO 1. Récupération de l'instance de lecture séquentielle du fichier de base csv
        // 2. Lecture de la ligne et parsage des différents champs contenus dans la ligne
        // 3. Désérialisation de l'image Base64 + écriture en clair dans le flux de restitution au format source
    }


    /**
     * Permet une désérialisation d'images vers de multiples support
     *
     * @param os
     */
    @Override
    public void setSourceOutputStream(OutputStream os) {
    }

    /**
     * Utile pour récupérer un Flux d'écriture sur la source à restituer
     *
     * @return
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return null;
    }

    /**
     * @param data
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(String data) throws IOException {
        return new Base64InputStream(new ByteArrayInputStream(data.getBytes())); // rajout
    }
}
