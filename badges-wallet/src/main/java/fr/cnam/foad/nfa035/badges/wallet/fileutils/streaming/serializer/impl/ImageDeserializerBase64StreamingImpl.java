package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;

/**
 * import de la classe ImageFrameMedia du package media
 */
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageDeserializer;
import org.apache.commons.codec.binary.Base64InputStream;
import java.io.*;
import java.util.function.ToDoubleBiFunction;

/**
 * Implémentation Base64 de désérialiseur d'image, basée sur des flux.
 */
public class ImageDeserializerBase64StreamingImpl extends AbstractStreamingImageDeserializer<AbstractImageFrameMedia> {

    private OutputStream sourceOutputStream;

    /**
     * Constructeur élémentaire
     * @param sourceOutputStream
     */
    public ImageDeserializerBase64StreamingImpl(OutputStream sourceOutputStream) {
        this.sourceOutputStream = sourceOutputStream;
    }

    /**
     * implémentation de classe abtraite AbstractStramingImageDeserializer
     * @return le flux d'écriture
     */
    @Override
    public OutputStream getSourceOutputStream() {
        return sourceOutputStream;
    }

    /**
     * implémentation de classe abtraite AbstractStramingImageDeserializer
     * @param os
     */
    @Override
    public void setSourceOutputStream(OutputStream os) {
        // TODO: 23/11/2022 corps de méthode ??
    }

    /**
     * Permet de récupérer le flux de lecture et de désérialisation à partir du media
     *
     * @param media
     * @return le flux de lecture
     * @throws IOException
     */
    /**
     * implementation de l'interface ImageStreamingDeserializer
     * @param media
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getDeserializingStream(ImageFrameMedia media) throws IOException {
        // TODO: 23/11/2022 corps de méthode ??
        return null;
    }

    /*    *//**
     * {@inheritDoc}
     *
     * @param media
     * @return le flux de lecture
     * @throws IOException
     *//*
    @Override
    public InputStream getDeserializingStream(AbstractImageFrameMedia media) throws IOException {
        return new Base64InputStream(media.getEncodedImageInput());
    }*/
}
