package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Classe abstraite visant à structurer/guider le développement dee manière rigoureuse
 * Elle s'applique à tout objet représentant un média de sérialisation.
 * Les implémentations sous-jacentes peuvent revêtir des aspects de fichiers ou bien de flux.
 *
 * @param <T>
 */
public abstract class AbstractImageFrameMedia<T> implements ImageFrameMedia {

    /**
     * le canal de distribution de notre image sérialisée, potentiellement un Fichier ou bien même un Flux
     */
    private T channel;

    /**
     * Constructeur vide pas utilisé
     */
    public AbstractImageFrameMedia() {
    }

    /**
     * Constructeur simple mais encapsulé "protected" > accessible uniquement via les classes filles
     * @param channel
     */
    protected AbstractImageFrameMedia(T channel){
        this.channel = channel;
    }


    /**
     * Permet d'obtenir le canal de distribution de notre image sérialisée, potentiellement un Fichier ou bien même un Flux
     *
     * @return le canal
     */
    @Override
    public T getChannel() { // surcharge méthode de l'interface
        return channel;
    }

    /**
     * Permet de définir le canal de distribution de notre image sérialisée, potentiellement un Fichier ou bien même un Flux
     *
     * @param channel
     */
    public void setChannel(T channel) {
        this.channel = channel;
    }

    /**
     * Permet d'obtenir le flux d'écriture sous-tendant à notre canal
     *
     * @return le flux d'écriture
     * @throws IOException
     */
    @Override
    public abstract OutputStream getEncodedImageOutput() throws IOException; //méthode abstraite surcharge méthode de l'interface

    /**
     * Permet d'obtenir le flux de lecture sous-tendant à notre canal
     *
     * @return le flux de lecture
     * @throws IOException
     */
    public abstract InputStream getEncodedImageInput() throws IOException; //méthode abstraite
}

