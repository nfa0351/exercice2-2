package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.IOException;
import java.io.OutputStream;

public interface ImageFrameMedia<T> {
    public  abstract T getChannel();
    public abstract OutputStream getEncodedImageOutput() throws IOException;
}
