package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

/**
 * import de la classe ImageFrameMedia du package media
 */
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;

import java.io.IOException;
import java.io.InputStream;

public interface DatabaseDeserializer<M extends ImageFrameMedia> extends BadgeDeserializer<M> {
    void deserialize(ResumableImageFrameMedia media) throws IOException;

    <K extends InputStream> K getDeserializingStream(String data) throws IOException;
}
