package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

/**
 * import de la classe ImageFrameMedia du package media
 */
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

/**
 * Interface définissant le comportement attendu d'un objet servant  à la désérialisation d'une image.
 * Ces objets doivent donc désérialiser à partir d'un média typé,
 * et il doit être possible d'en obtenir un flux de lecture pour l'obtention du contenu désérialisé.
 *
 * @param <M>
 */
public interface ImageStreamingDeserializer<M extends ImageFrameMedia> extends BadgeDeserializer {

    //@Override
    //public abstract void deserialize(ImageFrameMedia media) throws IOException;

    /**
     * Permet de récupérer le flux de lecture et de désérialisation à partir du media
     *
     * @param media
     * @param <K>
     * @return
     * @throws IOException
     */
    public abstract <K extends InputStream> K getDeserializingStream(M media) throws IOException;


    //<T extends OutputStream> T getSourceOutputStream();


   // <T extends OutputStream> void setSourceOutputStream(T os);




}
