package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.impl;

import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.T;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.AbstractImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ResumableImageFrameMedia;

import java.io.*;

/**
 * Implémentation d'ImageFrame pour un simple Fichier comme canal
 * implements interface ResumableImageFrameMedia 21/11/2022
 */
public class ResumableImageFileFrame extends AbstractImageFrameMedia<File> implements ResumableImageFrameMedia  { //File en type attendu

    private BufferedReader reader;

    public ResumableImageFileFrame(File walletDatabase) {
        super(walletDatabase);
    }

    /**
     * Méthode surchargée de la méthode abstraite de la classe AbstractImageFrameMedia
     * @return
     * @throws FileNotFoundException
     */
    @Override
    public OutputStream getEncodedImageOutput() throws FileNotFoundException {
        return new FileOutputStream(getChannel(), true);
    }

    /**
     * Méthode surchargée de la méthode abstraite de la classe AbstractImageFrameMedia
     * @return
     * @throws IOException
     */
    @Override
    public InputStream getEncodedImageInput() throws IOException {
        return new FileInputStream(getChannel());
    }


    /**
     * Implémentation de la méthode abstraite de l'interface ResumableImageFrameMedia
     */
    @Override
    public BufferedReader getEncodedImageReader(boolean resume) throws IOException {
        if (reader == null || !resume){
            this.reader = new BufferedReader(new InputStreamReader(new FileInputStream(getChannel())));
        }
        return this.reader;
    }
}
