package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media;

import java.io.BufferedReader;
import java.io.IOException;

/**
 * Interface qui hérite de ImageFrameMedia
 * @param <T>
 */
public abstract interface ResumableImageFrameMedia<T> extends ImageFrameMedia<T> {
    /**
     * interface de méthode implémenté par AbstractImageFrameMedia
     * @return
     */
    public abstract BufferedReader getEncodedImageReader(boolean resume) throws IOException;

    /**
     *  interface de méthode implémenté par AbstractImageFrameMedia
     * @return
     */
    public abstract T getChannel() ;






}
