package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer;

/**
 * import de la classe ImageFrameMedia du package media
 */
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Interface pour la désérialisation des images
 * @param <M> type de média
 */
public interface BadgeDeserializer<M extends ImageFrameMedia> {

    /**
     * Méthode principale de désérialisation, consistant au transfert simple du flux de lecture vers un flux d'écriture
     * @param media
     * @throws IOException
     */
    public abstract void deserialize(M media) throws IOException;

    /**
     * Utile pour récupérer un Flux d'écriture sur la source à restituer
     *
     * @param <T>
     * @return
     */
    public abstract <T extends OutputStream> T getSourceOutputStream();

    /**
     * Permet une désérialisation d'images vers de multiples support
     * @param os
     * @param <T>
     */
    public abstract <T extends OutputStream> void setSourceOutputStream(T os);
}
