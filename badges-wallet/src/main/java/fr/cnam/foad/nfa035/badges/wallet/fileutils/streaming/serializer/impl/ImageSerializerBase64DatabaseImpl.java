package fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.impl;


/**
 * import de la classe ImageFrameMedia du package media
 */
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.media.ImageFrameMedia;
import fr.cnam.foad.nfa035.badges.wallet.fileutils.streaming.serializer.AbstractStreamingImageSerializer;
import org.apache.commons.codec.binary.Base64OutputStream;

import java.io.*;
import java.nio.file.Files;

public class ImageSerializerBase64DatabaseImpl extends AbstractStreamingImageSerializer<File, ImageFrameMedia> {

    public InputStream getSourceInputStream(File source) throws FileNotFoundException {
        return new FileInputStream(source);
    }
    @Override
    public OutputStream getSerializingStream(ImageFrameMedia media) throws IOException {
        return new Base64OutputStream((OutputStream) media); // a tester !
    }
    @Override
    public final void serialize(File source, ImageFrameMedia media) throws IOException { //OK final pour classe concrète
        long numberOfLines = Files.lines(((File)media.getChannel()).toPath()).count();
        long size = Files.size(source.toPath());
        try(OutputStream os = media.getEncodedImageOutput()) { // a tester !
            Writer writer = new Writer() {
                @Override
                public void write(char[] cbuf, int off, int len) throws IOException {

                }

                @Override
                public void flush() throws IOException {

                }

                @Override
                public void close() throws IOException {

                }
            };
            writer.write(toString()); //a tester !!
            writer.flush();
            try(OutputStream eos = getSerializingStream(media)) {
                getSourceInputStream(source).transferTo(eos);
                writer.write("\n");
            }
            writer.flush();
        }
    }
}
